// Copyright (c) 2015 by Soleil Lapierre

#include <avr/io.h>
#include <avr/interrupt.h>

#include "types.h"

void (*gTimerCallback)(void) = NULL;
volatile int gResetTimeCount = FALSE;
volatile uint32_t gTimeCount = 0L;


void TimerEnable(void)
{
    // Use /64 clock prescaler. Combined with 8-bit timer counting to 250, this should
    // give an ISR rate of 100KHz. That's higher than strictly needed for the
    // keyboard and motor polling, but convenient for timing.
    TCCR0B |= (1 << CS01) | (1 << CS00);
    TCCR0A |= (1 << WGM01); // Reset timer count register on match.
    OCR0A = 249; // Interrupt every 1 / (16000000 / 64 / 250) = 1mS
    TIFR0 |= 1 << OCF0A; // Clear pending timer output compare interrupts.
    TIMSK0 |= 1 << OCIE0A; // Enable timer0 output compare interrupt.

    // Enable interrupts globally
    sei();
}


void TimerDisable(void)
{
    TIMSK0 &= ~(1 << OCIE0A); // Disable timer1 overflow interrupt.

    // Disable global interrupts (only using one of them anyway).
    cli();
}


void TimerSetCallback(void(*aFunc)(void))
{
    gTimerCallback = aFunc;
}


uint32_t TimerGetCount(void)
{
    return gTimeCount;
}


void TimerResetCount(void)
{
    gResetTimeCount = TRUE;
    while (gResetTimeCount == TRUE) {} // Block until the reset has actually occurred.
}


ISR(TIMER0_COMPA_vect)
{
    if (gTimerCallback != NULL)
    {
        gTimerCallback();
    }

    // Update timekeeping
    ++gTimeCount;
    if (gResetTimeCount == TRUE)
    {
        gTimeCount = 0L;
        gResetTimeCount = FALSE;
    }
}