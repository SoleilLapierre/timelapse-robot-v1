EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:TEENSYPP2
LIBS:Optrex_DMC20261
LIBS:L298
LIBS:basic_symbols
LIBS:breakbeam
LIBS:Linear_Timelapse_Robot_v1-cache
EELAYER 25 0
EELAYER END
$Descr User 7874 5906
encoding utf-8
Sheet 3 5
Title "Linear Timelapse Robot 1 Keyboard Interface"
Date ""
Rev "1"
Comp "Soleil Lapierre"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TEENSYPP2 U1
U 1 1 5591F52C
P 2000 2100
F 0 "U1" H 2000 2000 50  0000 C CNN
F 1 "TEENSYPP2" H 2000 2200 50  0000 C CNN
F 2 "MODULE" H 2000 2100 50  0001 C CNN
F 3 "DOCUMENTATION" H 2000 2100 50  0001 C CNN
	1    2000 2100
	1    0    0    -1  
$EndComp
$Comp
L TI_Keypad SW1
U 1 1 559217B6
P 6600 2800
F 0 "SW1" H 6600 1850 60  0000 C CNN
F 1 "TI_Keypad" H 6600 1750 60  0000 C CNN
F 2 "" H 6600 1750 60  0000 C CNN
F 3 "" H 6600 1750 60  0000 C CNN
	1    6600 2800
	1    0    0    -1  
$EndComp
$Comp
L 74LS138 U3
U 1 1 55921A57
P 4550 1400
F 0 "U3" H 4650 1900 60  0000 C CNN
F 1 "74LS138" H 4700 851 60  0000 C CNN
F 2 "" H 4550 1400 60  0000 C CNN
F 3 "" H 4550 1400 60  0000 C CNN
	1    4550 1400
	1    0    0    -1  
$EndComp
$Comp
L 74LS148 U4
U 1 1 55921AA2
P 4550 2850
F 0 "U4" H 4550 2850 60  0000 C CNN
F 1 "74LS148" H 4600 2600 60  0000 C CNN
F 2 "" H 4550 2850 60  0000 C CNN
F 3 "" H 4550 2850 60  0000 C CNN
	1    4550 2850
	-1   0    0    -1  
$EndComp
$Comp
L GNDREF #PWR7
U 1 1 55921D6F
P 3850 1950
F 0 "#PWR7" H 3850 1700 50  0001 C CNN
F 1 "GNDREF" H 3850 1800 50  0000 C CNN
F 2 "" H 3850 1950 60  0000 C CNN
F 3 "" H 3850 1950 60  0000 C CNN
	1    3850 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2350 3550 1050
Wire Wire Line
	3550 1050 3950 1050
Wire Wire Line
	3650 2450 3650 1150
Wire Wire Line
	3650 1150 3950 1150
Wire Wire Line
	3950 1250 3850 1250
Wire Wire Line
	3850 1250 3850 1950
Wire Wire Line
	3950 1650 3850 1650
Connection ~ 3850 1650
Wire Wire Line
	3950 1750 3850 1750
Connection ~ 3850 1750
Wire Wire Line
	6100 1050 6100 1850
Wire Wire Line
	6400 1150 6400 1850
Wire Wire Line
	6700 1250 6700 1850
Wire Wire Line
	7000 1350 7000 1850
Wire Wire Line
	5400 2750 5400 2450
Wire Wire Line
	5400 2450 5750 2450
Wire Wire Line
	5500 2750 5750 2750
Wire Wire Line
	5500 2750 5500 2850
Wire Wire Line
	5500 2950 5500 3050
Wire Wire Line
	5500 3050 5750 3050
Wire Wire Line
	5400 3050 5400 3350
Wire Wire Line
	5400 3350 5750 3350
Wire Wire Line
	5250 3150 5250 3650
Wire Wire Line
	5250 3650 5750 3650
Text Notes 6150 2400 0    60   ~ 0
1
Text Notes 6450 2400 0    60   ~ 0
2
Text Notes 6750 2400 0    60   ~ 0
3
Text Notes 7050 2400 0    60   ~ 0
Start
Text Notes 6150 2700 0    60   ~ 0
4
Text Notes 6450 2700 0    60   ~ 0
5
Text Notes 6750 2700 0    60   ~ 0
6
Text Notes 7050 2700 0    60   ~ 0
End
Text Notes 6150 3000 0    60   ~ 0
7
Text Notes 6450 3000 0    60   ~ 0
8
Text Notes 6750 3000 0    60   ~ 0
9
Text Notes 6450 3300 0    60   ~ 0
0
Text Notes 7050 3300 0    60   ~ 0
Enter
Text Notes 6750 3300 0    60   ~ 0
Up
Text Notes 6750 3600 0    60   ~ 0
Down
Text Notes 7050 3600 0    60   ~ 0
Right
Text Notes 6450 3600 0    60   ~ 0
Left
Text Notes 6150 3300 0    60   ~ 0
Clear
Text Notes 6150 3600 0    60   ~ 0
Reset
Wire Wire Line
	3350 2350 3550 2350
Wire Wire Line
	3650 2450 3350 2450
Wire Wire Line
	3350 2550 3950 2550
Wire Wire Line
	3950 2650 3350 2650
Wire Wire Line
	3350 2750 3950 2750
Wire Wire Line
	5150 1050 6100 1050
Wire Wire Line
	5150 1150 6400 1150
Wire Wire Line
	5150 1250 6700 1250
Wire Wire Line
	5150 1350 7000 1350
Wire Wire Line
	5150 3150 5250 3150
Wire Wire Line
	5150 3050 5400 3050
Wire Wire Line
	5150 2950 5500 2950
Wire Wire Line
	5500 2850 5150 2850
Wire Wire Line
	5150 2750 5400 2750
$EndSCHEMATC
