EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:TEENSYPP2
LIBS:Optrex_DMC20261
LIBS:L298
LIBS:basic_symbols
LIBS:breakbeam
LIBS:Linear_Timelapse_Robot_v1-cache
EELAYER 25 0
EELAYER END
$Descr User 7874 5906
encoding utf-8
Sheet 1 5
Title "Linear Timelapse Robot 1"
Date ""
Rev "1"
Comp "Soleil Lapierre"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1400 950  1450 900 
U 5591EFDC
F0 "LCD I/O and Camera Control" 60
F1 "LTR1v1_LCD.sch" 60
$EndSheet
Text Notes 4350 4150 0    60   ~ 0
This sheet is just a container for the others and can be ignored.
$Sheet
S 3650 1000 1450 850 
U 5591F523
F0 "Keyboard Interface" 60
F1 "LTR1x1_Keyboard.sch" 60
$EndSheet
$Sheet
S 1400 2450 1600 850 
U 55922802
F0 "Motor Interface" 60
F1 "LTR1v1_Motor.sch" 60
$EndSheet
$Sheet
S 3650 2450 1600 900 
U 55922805
F0 "Power Supply" 60
F1 "LTR1v1_Power.sch" 60
$EndSheet
$EndSCHEMATC
