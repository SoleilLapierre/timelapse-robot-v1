// Copyright (c) 2015 by Soleil Lapierre

#include <util/delay.h>

#include "keyboard.h"
#include "Teensy_HD44780.h"
#include "motor.h"
#include "camera.h"
#include "types.h"
#include "fixedstring.h"
#include "statemachine.h"
#include "motioncontrol.h"
#include "timer.h"
#include "menu.h"


#define PRODUCTNAME "Timelapse Robot 1"
#define VERSION_STR "version 1"
#define BY_STR      "by Soleil Lapierre"

typedef struct
{
	int mValue;     // Currently committed parameter value.
	int mDefault;   // Default value to reset to.
	int mPrecision; // How many decimal digits right of the decimal point.
	const char* mBlurb; // Prompt text.
    const char* mUnit;  // Unit symbol for value.
} MenuParams_t;

// Define state structures for the menus that edit values.
                                    //   val  default  dec  prompt                  units   (dec = decimal places)
MenuParams_t gMoveDirectionParams =     {  1,    1,     0,  NULL,                   NULL };
MenuParams_t gInitialVelocityParams =   {  0,    0,     0,  "Initial Velocity",     "mm/frame" };
MenuParams_t gAccelerationParams =      {  1,    1,     1,  "Acceleration",         "mm/f/f" };
MenuParams_t gMaxVelocityParams =       {  5,    5,     0,  "Max Velocity",         "mm/frame" };
MenuParams_t gSettleTimeParams =        {  5,    5,     1,  "Settle Time",          "s" };
MenuParams_t gTriggerPulseParams =      {  1,    1,     1,  "Trigger Pulse Time",   "s" };
MenuParams_t gExposureTimeParams =      {  8,    8,     1,  "Exposure Time",        "s" };
MenuParams_t gPhotoIntervalParams =     { 40,   40,     1,  "Photo Interval",       "s" };

// State machine OnEnter() callbacks for menus that manipulate data.
void* MoveDirectionMenu_OnEnter(void) { return &gMoveDirectionParams; }
void* InitialVelocityMenu_OnEnter(void) { return &gInitialVelocityParams; }
void* AccelerationMenu_OnEnter(void) { return &gAccelerationParams; }
void* MaxVelocityMenu_OnEnter(void) { return &gMaxVelocityParams; }
void* SettleTimeMenu_OnEnter(void) { return &gSettleTimeParams; }
void* TriggerPulseMenu_OnEnter(void) { return &gTriggerPulseParams; }
void* ExposureTimeMenu_OnEnter(void) { return &gExposureTimeParams; }
void* PhotoIntervalMenu_OnEnter(void) { return &gPhotoIntervalParams; }

// Generic OnLeave() callback waits for all keypresses to end.
void MenuNode_OnLeave(void* aUserState)
{
    (void)aUserState; // Parameter unused.
    KeyboardWaitForAllKeysUp();
}


// ----- Accessors for motion control system.

int MenuGetExposureDuration(void)
{
    return 100 * gExposureTimeParams.mValue; // Convert to milliseconds.
}


int MenuGetInitialVelocity(void)
{
    return gInitialVelocityParams.mValue; // mm/frame.
}


int MenuGetTriggerPulseDuraction(void)
{
    return 100 * gTriggerPulseParams.mValue; // Milliseconds
}


int MenuGetAcceleration(void)
{
    return gAccelerationParams.mValue; // units of 0.1 mm/frame/frame.
}


int MenuGetMaxVelocity(void)
{
    return gMaxVelocityParams.mValue; // mm/frame.
}


int MenuGetMotorDirection(void)
{
    return gMoveDirectionParams.mValue; // One of MOTOR_TOWARDS, MOTOR_AWAY or MOTOR_IDLE.
}


int MenuGetSettleTime(void)
{
    return 100 * gSettleTimeParams.mValue; // Milliseconds.
}


int MenuGetPhotoInterval(void)
{
    return 100 * gPhotoIntervalParams.mValue; // Milliseconds.
}

// ---- State machine DoWork() callbacks.

// About screen that displays product info on a scrolling line.
int AboutMenu_DoWork(void* aUserState)
{
	const char* scrolly = PRODUCTNAME " " VERSION_STR " " BY_STR;
	int length;
	int i;
	int scrollPos = 20;
	byte key = KEY_NONE;

    (void)aUserState;

	LCDClear();
	LCDSetCursorAddress((20 - 6) / 2);
	LCDWriteString("ABOUT:");

	// Find the length of the scrolling string.
	length = 0;
	while (scrolly[length] != '\0')
	{
		++length;
	}

	do
	{
		LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);

		// Write spaces up to the start of the scrolling string.
		i = 0;
		while (i < scrollPos)
		{
			LCDWriteChar(' ');
			++i;
		}

		// Write scrolling string until its end or end of screen space.
		while ((i < 20) && (i - scrollPos < length))
		{
			LCDWriteChar(scrolly[i - scrollPos]);
			++i;
		}

		// Write spaces until end of screen space.
		while (i < 20)
		{
			LCDWriteChar(' ');
			++i;
		}

		// Update scroll position and wrap around when string is offscreen.
		--scrollPos;
		if (scrollPos < -length)
		{
			scrollPos = 20;
		}

		// Scrolling animation delay.
		_delay_ms(333);

		// Any key press ends the display.
		key = KeyboardQueuePop();

	} while ((key == KEY_NONE) && (LCDGetStatus() == 0));

	if (key == KEY_UP)
	{
        return MENUSIGNAL_PREV;
	}

    return MENUSIGNAL_NEXT;
}


// Manually move the bogey with the start/end keys.
int MotorTestMenu_DoWork(void* aUserState)
{
    int result = MENUSIGNAL_NONE;
	int showCount = FALSE;

    (void)aUserState;
	LCDClear();
	LCDWriteString("Manual Move:");
	LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
	LCDWriteString("Use Start/End keys.");
	LCDEnableCursor(0);
	LCDEnableCursorBlink(0);

	do
	{
		if (KeyboardIsKeyPressed(KEY_START) != 0)
		{
			showCount = TRUE;
			MotorStart(MOTOR_TOWARDS);
		}
		else if (KeyboardIsKeyPressed(KEY_END) != 0)
		{
			showCount = TRUE;
			MotorStart(MOTOR_AWAY);
		}
		else
		{
			MotorStop();
			if (KeyboardIsKeyPressed(KEY_UP))
			{
                result = MENUSIGNAL_PREV;
			}
			else if (KeyboardIsKeyPressed(KEY_DOWN))
			{
                result = MENUSIGNAL_NEXT;
			}
		}

		if (showCount == TRUE)
		{
			LCDSetCursorAddress(16);
			LCDPrintHex4(MotorGetSensorCount());
		}

		KeyboardQueueClear();

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

	return result;
}


int TimerTestMenu_DoWork(void* aUserState)
{
    int result = MENUSIGNAL_NONE;
    uint32_t time = 0;
    byte* bytes = (byte*)&time; // This platform doesn't properly support bit shifting on 32-bit values.
    int i;

    (void)aUserState;
    LCDClear();
    LCDWriteString("Current time:");
    LCDEnableCursor(0);
    LCDEnableCursorBlink(0);

    do
    {
        LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
        time = TimerGetCount();
        for (i = 3; i >= 0; --i)
        {
            LCDPrintHex2(bytes[i]);
        }
        
        switch (KeyboardQueuePop())
        {
            case KEY_RESET:
                TimerResetCount();
                break;
            case KEY_UP:
                result = MENUSIGNAL_PREV;
                break;
            case KEY_DOWN:
                result = MENUSIGNAL_NEXT;
                break;
            default: break;
        }

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

    return result;
}


// Test keyboard scancodes (for debugging).
int KeyboardTestMenu_DoWork(void* aUserState)
{
    int result = MENUSIGNAL_NONE;
	int onePress = 0;
	byte keyCode = 0;

    (void)aUserState;
	LCDClear();
	LCDWriteString("Keyboard Test:");
	LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
	LCDWriteString("Press Start to begin");
	LCDEnableCursor(0);
	LCDEnableCursorBlink(0);

	do
	{
		if (KeyboardIsKeyPressed(KEY_START) != 0)
		{
            result = MENUSIGNAL_GO;
		}
		else if (KeyboardIsKeyPressed(KEY_UP))
		{
            result = MENUSIGNAL_PREV;
		}
		else if (KeyboardIsKeyPressed(KEY_DOWN))
		{
            result = MENUSIGNAL_NEXT;
		}

		KeyboardQueueClear();

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

    if (result != MENUSIGNAL_GO)
	{
		return result;
	}

    result = MENUSIGNAL_NONE;

	LCDClear();
	LCDWriteString("Keyboard Test:");
	LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
	LCDWriteString("Exit:Press End twice");

	do
	{
		if (KeyboardQueuePeek() != KEY_NONE)
		{
			keyCode = KeyboardQueuePop();
			if (keyCode == KEY_END)
			{
				++onePress;
				if (onePress > 1)
				{
                    result = MENUSIGNAL_REDO;
				}
			}
			else
			{
				onePress = 0;
			}
		}

		LCDSetCursorAddress(15);
		LCDPrintHex2(keyCode);

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

	return result;
}


// Test fire the camera (for debugging)
int CameraTestMenu_DoWork(void* aUserState)
{
    int result = MENUSIGNAL_NONE;

    (void)aUserState;
	LCDClear();
	LCDWriteString("Camera Test: Press");
	LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
	LCDWriteString("Start to trigger.");
	LCDEnableCursor(0);
	LCDEnableCursorBlink(0);

	do
	{
		if (KeyboardIsKeyPressed(KEY_START) != 0)
		{
			CameraTrigger();
		}
		else if (KeyboardIsKeyPressed(KEY_UP))
		{
            result = MENUSIGNAL_PREV;
		}
		else if (KeyboardIsKeyPressed(KEY_DOWN))
		{
            result = MENUSIGNAL_NEXT;
		}

		KeyboardQueueClear();

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

	return result;
}


// Menu for editing fixed-point motion parameters.
int EditValueMenu_DoWork(void* aUserState)
{
    MenuParams_t* params = (MenuParams_t*)aUserState;
    byte result = MENUSIGNAL_NONE;
	byte key;
	float value = params->mValue;
	const int LEFT = 4;          // Leftmost allowed cursor position on the second line.
	char buf[16];
	int bufLen = 0;
	int cursorPos = 0;
    int lastCursorDir = 1;
    int valueChanged = FALSE;
    int refresh = TRUE;

	LCDClear();
	if (params->mBlurb != NULL)
	{
		LCDWriteString(params->mBlurb);
	}

	LCDEnableCursor(1);
	LCDEnableCursorBlink(1);

	do
	{
        // Note that negative values are currently not handled.

        if (refresh == TRUE) // Only update display when a change occurs; causes flicker otherwise.
        {
            buf[0] = ' '; // Always place a leading space to allow expanding the value.
            FixedToString(value, params->mPrecision, buf + 1);
		    bufLen = 0;
            while (buf[bufLen] != '\0')
            {
                ++bufLen;
            }

            if (cursorPos > bufLen - 1)
            {
                cursorPos = bufLen - 1;
            }

		    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START + LEFT);
		    LCDWriteString(buf);

            // Write unit string if any.
            if (params->mUnit != NULL)
            {
                LCDWriteChar(' ');
                LCDWriteString(params->mUnit);
                LCDWriteString("      "); // Erase cruft that might be left by drastic number length changes.
            }

            if (buf[cursorPos] == '.') // Do not allow user to overwrite the decimal point, as the keyboard doesn't have one.
            {
                cursorPos += lastCursorDir; // FixedToString() guarantees a digit on both sides of the decimal point.
            }

            // Place the cursor at the current edit point.
		    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START + LEFT + cursorPos);
        }

		// Handle input.
        valueChanged = FALSE;
        refresh = FALSE;
		key = KeyboardQueuePop();
        
		switch (key)
		{
            case KEY_0: // Deliberate fall-through.
            case KEY_1: 
            case KEY_2: 
            case KEY_3: 
            case KEY_4: 
            case KEY_5: 
            case KEY_6: 
            case KEY_7: 
            case KEY_8: 
            case KEY_9: 
                buf[cursorPos] = '0' + KeyboardNumericValue(key); 
                valueChanged = TRUE; 
                break;
            case KEY_UP: // Change menu; abandon current edit.
                result = MENUSIGNAL_PREV;
                break;
            case KEY_DOWN:
                result = MENUSIGNAL_NEXT;
				break;
            case KEY_LEFT: // Cursor move.
                if (cursorPos > 0)
                {
                    --cursorPos;
                    lastCursorDir = -1;
                    refresh = TRUE;
                }
                break;
            case KEY_RIGHT: // Cursor move.
                if (cursorPos < bufLen - 1)
                {
                    ++cursorPos;
                    lastCursorDir = 1;
                    refresh = TRUE;
                }
                break;
            case KEY_ENTER: // Commit value to working memory and move to next menu.
                params->mValue = value;
                result = MENUSIGNAL_NEXT;
                break;
            case KEY_CLEAR: // Zero out the value.
                value = 0;
                refresh = TRUE;
                cursorPos = 0;
                break;
            case KEY_RESET: // Reset to default.
                value = params->mDefault;
                refresh = TRUE;
                cursorPos = 0;
                break;
			default: // Other keys not handled.
				break;
		}

        if (valueChanged == TRUE)
        {
            value = StringToFixed(buf);
            if (cursorPos < bufLen - 1) // A digit was entered, so move the cursor right.
            {
                ++cursorPos;
            }
            lastCursorDir = 1; 
            refresh = TRUE;
        }

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

	return result;
}


// Change the bogey movement direction.
int MoveDirectionMenu_DoWork(void* aUserState)
{
    MenuParams_t* params = (MenuParams_t*)aUserState;
    int result = MENUSIGNAL_NONE;
    int changed = TRUE;
    byte key = KEY_NONE;

    LCDClear();
    LCDWriteString("Movement Direction");
    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
    LCDWriteString("Change \x7F\x7E:");
    LCDEnableCursor(0);
    LCDEnableCursorBlink(0);

    do
    {
        if (changed == TRUE)
        {
            LCDSetCursorAddress(TEENSY_HD44780_LINE2_START + 11);
            switch (params->mValue)
            {
                case -1: LCDWriteString("Towards"); break;
                 case 1: LCDWriteString("Away   "); break;
                default: LCDWriteString("None   "); break;
            }
        }

        changed = FALSE;
        key = KeyboardQueuePop();
        switch (key)
        {
            case KEY_RIGHT:
                changed = TRUE;
                params->mValue++;
                if (params->mValue > 1)
                {
                    params->mValue = -1;
                }
                break;
            case KEY_LEFT:
                changed = TRUE;
                params->mValue--;
                if (params->mValue < -1)
                {
                    params->mValue = 1;
                }
                break;
            case KEY_UP:
                result = MENUSIGNAL_PREV;
                break;
            case KEY_DOWN:
            case KEY_ENTER: // deliberate fallthrough.
                result = MENUSIGNAL_NEXT;
                break;
            case KEY_RESET:
                changed = TRUE;
                params->mValue = params->mDefault;
                break;
            case KEY_CLEAR:
                changed = TRUE;
                params->mValue = 0;
                break;
            default: break;
        }

    } while ((result == MENUSIGNAL_NONE) && (LCDGetStatus() == 0));

    return result;
}


// Do the actual motion sequence. This is implemented as a state machine in another module.
int RunProgramMenu_DoWork(void* aUserState)
{
    StateMachineState_t stateMachine;
    int result = MENUSIGNAL_NONE;

    (void)aUserState;

    MotionControlInit(&stateMachine);

    while (result == MENUSIGNAL_NONE)
    {
        result = MotionControlUpdate(&stateMachine);
    }

    return result;
}


// Remember to update these indices when changing the state machine definition below!
enum
{
    ABOUT_MENU_NODE,
    MOVE_DIRECTION_MENU_NODE,
    INITIAL_VELOCITY_MENU_NODE,
    ACCELERATION_MENU_NODE,
    MAX_VELOCITY_MENU_NODE,
    SETTLE_TIME_MENU_NODE,
    TRIGGER_PULSE_MENU_NODE,
    EXPOSURE_TIME_MENU_NODE,
    PHOTO_INTERVAL_MENU_NODE,
    RUN_PROGRAM_MENU_NODE,
    MOTOR_TEST_MENU_NODE,
    KEYBOARD_TEST_MENU_NODE,
    CAMERA_TEST_MENU_NODE,
    TIMER_TEST_MENU_NODE,
    NUM_MENUS
};

// State transition lists.
StateMachineNextState_t gAboutMenuTransitions[] = 
{
    { MENUSIGNAL_PREV, TIMER_TEST_MENU_NODE },
    { MENUSIGNAL_NEXT, MOVE_DIRECTION_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gMoveDirectionMenuTransitions[] =
{
    { MENUSIGNAL_PREV, ABOUT_MENU_NODE },
    { MENUSIGNAL_NEXT, INITIAL_VELOCITY_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gInitialVelocityMenuTransitions[] = 
{ 
    { MENUSIGNAL_PREV, MOVE_DIRECTION_MENU_NODE },
    { MENUSIGNAL_NEXT, ACCELERATION_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gAccelerationMenuTransitions[] = 
{ 
    { MENUSIGNAL_PREV, INITIAL_VELOCITY_MENU_NODE },
    { MENUSIGNAL_NEXT, MAX_VELOCITY_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gMaxVelocityMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, ACCELERATION_MENU_NODE },
    { MENUSIGNAL_NEXT, SETTLE_TIME_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gSettleTimeMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, MAX_VELOCITY_MENU_NODE },
    { MENUSIGNAL_NEXT, TRIGGER_PULSE_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gTriggerPulseMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, SETTLE_TIME_MENU_NODE },
    { MENUSIGNAL_NEXT, EXPOSURE_TIME_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gExposureTimeMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, TRIGGER_PULSE_MENU_NODE },
    { MENUSIGNAL_NEXT, PHOTO_INTERVAL_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gPhotoIntervalMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, EXPOSURE_TIME_MENU_NODE },
    { MENUSIGNAL_NEXT, RUN_PROGRAM_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gRunProgramMenuTransitions[] = 
{
    { MENUSIGNAL_PREV, PHOTO_INTERVAL_MENU_NODE },
    { MENUSIGNAL_NEXT, MOTOR_TEST_MENU_NODE },
    { MENUSIGNAL_REDO, RUN_PROGRAM_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gMotorTestMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, RUN_PROGRAM_MENU_NODE },
    { MENUSIGNAL_NEXT, KEYBOARD_TEST_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gKeyboardTestMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, MOTOR_TEST_MENU_NODE },
    { MENUSIGNAL_NEXT, CAMERA_TEST_MENU_NODE },
    { MENUSIGNAL_REDO, KEYBOARD_TEST_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gCameraTestMenuTransitions[] =
{ 
    { MENUSIGNAL_PREV, KEYBOARD_TEST_MENU_NODE },
    { MENUSIGNAL_NEXT, TIMER_TEST_MENU_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gTimerTestMenuTransitions[] =
{
    { MENUSIGNAL_PREV, CAMERA_TEST_MENU_NODE },
    { MENUSIGNAL_NEXT, ABOUT_MENU_NODE },
    TRANSITIONTABLE_END
};


// State machine for menu system.
StateMachineNode_t gMenuStateMachine[] =
{  // OnEnter()                     DoWork()                    OnLeave()           Transition table
    { NULL,                         AboutMenu_DoWork,           MenuNode_OnLeave,   gAboutMenuTransitions },
    { MoveDirectionMenu_OnEnter,    MoveDirectionMenu_DoWork,   MenuNode_OnLeave,   gMoveDirectionMenuTransitions },
    { InitialVelocityMenu_OnEnter,  EditValueMenu_DoWork,       MenuNode_OnLeave,   gInitialVelocityMenuTransitions },
    { AccelerationMenu_OnEnter,     EditValueMenu_DoWork,       MenuNode_OnLeave,   gAccelerationMenuTransitions },
    { MaxVelocityMenu_OnEnter,      EditValueMenu_DoWork,       MenuNode_OnLeave,   gMaxVelocityMenuTransitions },
    { SettleTimeMenu_OnEnter,       EditValueMenu_DoWork,       MenuNode_OnLeave,   gSettleTimeMenuTransitions },
    { TriggerPulseMenu_OnEnter,     EditValueMenu_DoWork,       MenuNode_OnLeave,   gTriggerPulseMenuTransitions },
    { ExposureTimeMenu_OnEnter,     EditValueMenu_DoWork,       MenuNode_OnLeave,   gExposureTimeMenuTransitions },
    { PhotoIntervalMenu_OnEnter,    EditValueMenu_DoWork,       MenuNode_OnLeave,   gPhotoIntervalMenuTransitions },
    { NULL,                         RunProgramMenu_DoWork,      MenuNode_OnLeave,   gRunProgramMenuTransitions },
    { NULL,                         MotorTestMenu_DoWork,       MenuNode_OnLeave,   gMotorTestMenuTransitions },
    { NULL,                         KeyboardTestMenu_DoWork,    MenuNode_OnLeave,   gKeyboardTestMenuTransitions },
    { NULL,                         CameraTestMenu_DoWork,      MenuNode_OnLeave,   gCameraTestMenuTransitions },
    { NULL,                         TimerTestMenu_DoWork,       MenuNode_OnLeave,   gTimerTestMenuTransitions }
};


// Sets up and runs the menu state machine.
int MainMenu(void)
{
    StateMachineState_t stateMachine;
	int currentNode = 0;
	int error = 0;

    StateMachineInit(gMenuStateMachine, NUM_MENUS, ABOUT_MENU_NODE, &stateMachine);

    error |= LCDGetStatus();
    while (error == 0)
	{
		while (KeyboardIsAnyKeyPressed() != 0) { } // Wait for end of keypress that terminated last menu.

        currentNode = StateMachineUpdate(&stateMachine);

        if (currentNode < 0)
        {
            // Prevent overlap between state machine error codes and LCD error codes.
            error |= (-currentNode << 2); // - because state machine error codes are always negative.
            // Don't need to forcibly halt the menu state machine since none of
            // the OnEnter() functions allocate resources.
        }

		error |= LCDGetStatus();
	}

	// Error detected; try to output an error message.
	MotorStop();
	LCDClear();
	LCDWriteString("ERROR - ABORTING");
	LCDSetCursorAddress(TEENSY_HD44780_LINE2_START + 7);
	LCDPrintHex2(error);

    return error;
}
