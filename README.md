# Timelapse Robot

Code and schematics for my first attempt at building a cheap linear rail for adding motion to timelapse video.

Project documentation, photos and video can be found on [my website](https://www.soleillapierre.ca/projects/hardware/ltr1v1/).
