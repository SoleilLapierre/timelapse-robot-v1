// Copyright (c) 2015 by Soleil Lapierre

#ifndef _MOTIONCONTROL_H_
#define _MOTIONCONTROL_H_

void MotionControlInit(StateMachineState_t* aStateMachine);
int MotionControlUpdate(StateMachineState_t* aStateMachine);

#endif
