// Copyright (c) 2015 by Soleil Lapierre

#include <util/delay.h>
#include <avr/io.h>

#include "debug.h"
#include "Teensy_HD44780.h"
#include "keyboard.h"
#include "menu.h"
#include "motor.h"
#include "camera.h"
#include "timer.h"

// Teensy 2.0: LED is active high
#if defined(__AVR_ATmega32U4__) || defined(__AVR_AT90USB1286__)
#define LED_ON		(PORTD |= (1<<6))
#define LED_OFF		(PORTD &= ~(1<<6))

// Teensy 1.0: LED is active low
#else
#define LED_ON	(PORTD &= ~(1<<6))
#define LED_OFF	(PORTD |= (1<<6))
#endif

#define LED_CONFIG	(DDRD |= (1<<6))
#define CPU_PRESCALE(n)	(CLKPR = 0x80, CLKPR = (n))

// Timer ISR callback for doing keyboard and motor polling.
volatile int gPollCounter = 0;

void TimerCallback(void)
{
    // Only poll keyboard and motor sensors at 250Hz.
    ++gPollCounter;
    if ((gPollCounter & 0x0003) == 0)
    {
        KeyboardUpdate();
        MotorSensorPoll();
    }
}


// ----- MAIN -----
int main(void)
{
    int errorCode = 0;
    int i = 0;

    // set for 16 MHz clock, and make sure the LED is off
    CPU_PRESCALE(0);
    LED_CONFIG;
    LED_OFF;
	DEBUG_INIT();
	MotorInit();
	CameraInit();
	KeyboardInit(1);

// Wait for PC USB initialization.
#ifdef DEBUG_MODE
    _delay_ms(4000);
#else
    _delay_ms(1000);
#endif

	DEBUG_PRINT("----- Initializing LCD.\n"); DEBUG_FLUSH();
	LCDInit();

    TimerSetCallback(TimerCallback);
	TimerEnable();

	DEBUG_PRINT("----- Entering main loop.\n"); DEBUG_FLUSH();
    errorCode = MainMenu(); // This should only return on error.

    // Signal end of program by blinking LCD.
	TimerDisable();
	DEBUG_PRINT("----- Detected an error; flashing LED.\n"); DEBUG_FLUSH();
	while (1)
    {
        // Indicate the error code by blinking the LED that number of times, with a pause between.
        i = 0;
        while (i < errorCode)
        {
            LED_ON;
	        _delay_ms(100);
	        LED_OFF;
	        _delay_ms(100);
            ++i;
        }

        _delay_ms(500);
    }
}



