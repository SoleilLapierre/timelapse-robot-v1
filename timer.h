// Copyright (c) 2015 by Soleil Lapierre

#ifndef _TIMER_H_
#define _TIMER_H_

void TimerEnable(void);
void TimerDisable(void);

void TimerSetCallback(void (*aFunc)(void));

uint32_t TimerGetCount(void);
void TimerResetCount(void);

#endif
