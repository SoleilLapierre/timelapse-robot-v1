// Copyright (c) 2015 by Soleil Lapierre

#ifndef _CAMERA_H_
#define _CAMERA_H_

// Called once at program start.
void CameraInit(void);

// Call to set the duration in milliseconds of the trigger pulse.
void CameraSetTriggerPulseWidth(int aMilliseconds);

// Call to trip the camera shutter. This blocks for the duration of the trigger pulse.
void CameraTrigger(void);

#endif
