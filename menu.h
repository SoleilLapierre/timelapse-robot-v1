// Copyright (c) 2015 by Soleil Lapierre

#ifndef _MENU_H_
#define _MENU_H_

// Menu navigation signals - the motion control system needs these to pass back results.
enum
{
    MENUSIGNAL_NONE,
    MENUSIGNAL_NEXT,
    MENUSIGNAL_PREV,
    MENUSIGNAL_REDO,
    MENUSIGNAL_GO
};

int MainMenu(void);

// Called by the motion control system to get user-defined parameters.
int MenuGetExposureDuration(void);
int MenuGetInitialVelocity(void);
int MenuGetTriggerPulseDuraction(void);
int MenuGetAcceleration(void);
int MenuGetMaxVelocity(void);
int MenuGetMotorDirection(void);
int MenuGetSettleTime(void);
int MenuGetPhotoInterval(void);

#endif
