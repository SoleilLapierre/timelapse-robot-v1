// Copyright (c) 2015 by Soleil Lapierre

#include <avr/io.h>
#include <util/delay.h>

// Note Port C is shared with the LCD I/O.
#define CAMERA_DDR DDRC
#define CAMERA_PORT_WRITE PORTC

// Bit 7 is the camera trigger (active high)
#define CAMERA_BIT    0x80

// Length of the trigger pulse in mS
int gCameraPulseWidth = 100;

void CameraInit(void)
{
	CAMERA_DDR |= CAMERA_BIT;						    
	CAMERA_PORT_WRITE &= ~CAMERA_BIT;
}

void CameraSetTriggerPulseWidth(int aMilliseconds)
{
    gCameraPulseWidth = aMilliseconds;
}

void CameraTrigger(void)
{
	CAMERA_PORT_WRITE |= CAMERA_BIT;
    _delay_ms(gCameraPulseWidth);
	CAMERA_PORT_WRITE &= ~CAMERA_BIT;
}

