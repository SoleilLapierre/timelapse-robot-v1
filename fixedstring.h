// Copyright (c) 2015 by Soleil Lapierre

#ifndef _FIXEDSTRING_H_
#define _FIXEDSTRING_H_

// Routines for converting between fixed point and strings.

void FixedToString(int aValue, int aPrecision, char* aOutput);
int StringToFixed(const char* aBuffer);

#endif
