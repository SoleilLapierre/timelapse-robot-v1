// Copyright (c) 2015 by Soleil Lapierre

#ifndef _STATEMACHINE_H_
#define _STATEMACHINE_H_

// Key-value pair defining transitions out of a node.
typedef struct
{
    int mSignal;   // Nonzero signal returned by DoWork(). 
    int mNewIndex; // New state index to switch to if this signal is received.
                   // Note that if the current node is given as a transition, OnLeave() and OnEnter() will be called.
} StateMachineNextState_t;

#define TRANSITIONTABLE_END  { -1, -1 }


// The content of a state machine node: processing functions, and transitions out.
typedef struct
{
    // Function pointers can be NULL, in which case they aren't called.
    void* (*OnEnter)(void);             // Called when entering the state. Optionally returns state info to pass to DoWork().
    int (*DoWork)(void* aUserState);    // Called repeatedly to do processing in the current state. 0 means state in current state without transitioning.
    void (*OnLeave)(void* aUserState);  // Called when leaving the state. Should free resources allocated by OnEnter().
    StateMachineNextState_t* mNext;     // Transitions out of this state. Must be terminated with an entry having mSignal < 0.
} StateMachineNode_t;

// State machine state tracking.
typedef struct
{
    StateMachineNode_t* mNodeTable; // Saved pointer to state machine definition.
    int mNumNodes;                  // Size of definition.
    int mCurrentNode;               // Current node index.
    int mTransitioning;             // True if we just transitioned to a new state.
    void* mUserState;               // User state info returned by OnEnter().
} StateMachineState_t;


// Initialize a state machine. Initializes the StateMachineState_t data structure.
void StateMachineInit(StateMachineNode_t* aNodes, int aNumNodes, int aFirstNode, StateMachineState_t* aStateMachine);

// Update the state machine. Returns the current node index.
// Caller must check the return value for out of range - this indicates the state machine has halted.
// You can encode a deliberate halt transition in the mNext table of a node by using an invalid mNewIndex.
int StateMachineUpdate(StateMachineState_t* aStateMachine);

// Force a transition out of the current state. Same returns as StateMachineUpdate().
// Does not call DoWork() but does call OnLeave().
// This essentially simulates a given return value from DoWork().
int StateMachineForceTransition(StateMachineState_t* aStateMachine, int aSignal);

// State machine error codes.
// You can define your own halt state codes by transitioning to node indices that
// are out of range and not one of these values.
#define STATEMACHINE_INVALID -1
#define STATEMACHINE_UNHANDLED_SIGNAL -2

#endif
