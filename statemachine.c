// Copyright (c) 2015 by Soleil Lapierre

#include "statemachine.h"
#include "types.h"

void StateMachineInit(StateMachineNode_t* aNodes, int aNumNodes, int aFirstNode, StateMachineState_t* aStateMachine)
{
    aStateMachine->mNodeTable = aNodes;
    aStateMachine->mNumNodes = aNumNodes;
    aStateMachine->mCurrentNode = aFirstNode;
    aStateMachine->mTransitioning = TRUE;
}


int StateMachineUpdate(StateMachineState_t* aStateMachine)
{
    int signal = 0;
    int i = 0;
    int newNode = aStateMachine->mCurrentNode;

    if ((aStateMachine->mCurrentNode < 0) || (aStateMachine->mCurrentNode >= aStateMachine->mNumNodes))
    {
        return aStateMachine->mCurrentNode; // Machine has halted.
    }

    if (aStateMachine->mNodeTable == NULL)
    {
        return STATEMACHINE_INVALID; // Invalid state transition table.
    }

    // Get current node.
    const StateMachineNode_t* curNode = &(aStateMachine->mNodeTable[aStateMachine->mCurrentNode]);

    // Perform OnEnter() if necessary.
    if (aStateMachine->mTransitioning == TRUE)
    {
        aStateMachine->mTransitioning = FALSE;

        if (curNode->OnEnter != NULL)
        {
            aStateMachine->mUserState = curNode->OnEnter();
        }
    }

    // Update current node.
    if (curNode->DoWork != NULL)
    {
        signal = curNode->DoWork(aStateMachine->mUserState);
        aStateMachine->mUserState = NULL;
    }

    // Transition to a new node if we received a signal. (signals must be positive or zero)
    if (signal >= 0)
    {
        // Receiving a nonzero signal always causes a transition, so leave the current node.
        if (curNode->OnLeave != NULL)
        {
            curNode->OnLeave(aStateMachine->mUserState);
        }

        // Search for the signal in the node's transition table.
        if (curNode->mNext != NULL)
        {
            i = 0; 
            while (curNode->mNext[i].mSignal >= 0)
            {
                if (curNode->mNext[i].mSignal == signal)
                {
                    newNode = curNode->mNext[i].mNewIndex;
                    aStateMachine->mTransitioning = TRUE;
                    break;
                }

                ++i;
            }
        }

        // If transitioning, leave the current node.
        if (aStateMachine->mTransitioning == TRUE)
        {
            aStateMachine->mCurrentNode = newNode;
        }
        else
        {
            aStateMachine->mCurrentNode = STATEMACHINE_UNHANDLED_SIGNAL; // Unhandled signal; halts the state machine.
        }
    }

    return aStateMachine->mCurrentNode;
}


int StateMachineForceTransition(StateMachineState_t* aStateMachine, int aSignal)
{
    int i = 0;
    int newNode = aStateMachine->mCurrentNode;

    if ((aStateMachine->mCurrentNode < 0) || (aStateMachine->mCurrentNode >= aStateMachine->mNumNodes))
    {
        return aStateMachine->mCurrentNode; // Machine has halted.
    }

    if (aStateMachine->mNodeTable == NULL)
    {
        return STATEMACHINE_INVALID; // Invalid state transition table.
    }

    // Get current node.
    const StateMachineNode_t* curNode = &(aStateMachine->mNodeTable[aStateMachine->mCurrentNode]);

    // Receiving a forced signal always causes a transition, so leave the current node.
    if (curNode->OnLeave != NULL)
    {
        curNode->OnLeave(aStateMachine->mUserState);
    }

    // Search for the signal in the node's transition table.
    if (curNode->mNext != NULL)
    {
        i = 0;
        while (curNode->mNext[i].mSignal >= 0)
        {
            if (curNode->mNext[i].mSignal == aSignal)
            {
                newNode = curNode->mNext[i].mNewIndex;
                aStateMachine->mTransitioning = TRUE;
                break;
            }

            ++i;
        }
    }

    // If transitioning, leave the current node.
    if (aStateMachine->mTransitioning == TRUE)
    {
        aStateMachine->mCurrentNode = newNode;
    }
    else
    {
        aStateMachine->mCurrentNode = STATEMACHINE_UNHANDLED_SIGNAL; // Unhandled signal; halts the state machine.
    }

    return aStateMachine->mCurrentNode;
}
