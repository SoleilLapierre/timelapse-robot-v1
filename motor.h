// Copyright (c) 2015 by Soleil Lapierre

#ifndef _MOTOR_H_
#define _MOTOR_H_

#include <avr/io.h>

#define MOTOR_IDLE 0
#define MOTOR_TOWARDS -1
#define MOTOR_AWAY 1


// Called once at program start.
void MotorInit(void);

// Pass in MOTOR_TOWARDS to move towards start (the motor end of the shaft) or MOTOR_AWAY to move towards the end.
// Other values have no effect, meaning the motor will keep running if it already is.
// If the motor is already moving in the other direction, this functino will
// stop the motor and block for 100mS before reversing direction.
void MotorStart(int aDirection);

void MotorStop(void);

void MotorSensorPoll(void); // Called by timer ISR.

// Returns motor rotation sensor count since last counter reset.
// Negative values indicate motion towards the motor end of the shaft.
// There are 16 ticks per rotation and 20 rotations per inch of linear movement.
int MotorGetSensorCount(void);

void MotorSensorCountReset(void); // Results undefined if called while motor is running.

#endif
