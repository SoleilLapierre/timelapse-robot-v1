// Copyright (c) 2015 by Soleil Lapierre

#include <string.h>
#include <stdio.h>
#include <util/delay.h>

#include "types.h"
#include "motor.h"
#include "camera.h"
#include "keyboard.h"
#include "Teensy_HD44780.h"
#include "timer.h"
#include "statemachine.h"
#include "menu.h"


// Variables related to the motion control sequencing.
typedef struct
{
    int mMenuResultCode;    // Result to pass back up to menu system.
    uint16_t mPictureCount; // Number of photos taken.
    uint32_t mBaseTime;     // Start time of current photo iteration (in mS relative to initiating the camera shutter).
    int mPhotoInterval;     // Time between pictures in mS.
    int mPhase;             // 1 = accelerating, 0 = coasting, -1 = decelerating.
    int mEndAfterNextPhoto; // True if the motion control program has reached the last frame.
    int mVelocity;          // Current per-frame move distance in units of 0.1mm/frame.
    int mAcceleration;      // Acceleration/deceleration change in velocity in units of 0.1mm/frame/frame.
    int mMaxVelocity;       // Maximum move distance in units of 0.1mm/frame.
    int mMotorDirection;    // One of MOTOR_TOWARDS, MOTOR_AWAY or MOTOR_IDLE.
    int mMotorSettleTime;   // Time in mS to wait for physical vibration to stop after stopping the motor.
    const char* mAbortPrompt; // Message to display on second line of LCD.
} MotionControlState_t;

MotionControlState_t gMotionControlState;

#define PHASE_ACCELERATING 1
#define PHASE_COASTING 0
#define PHASE_DECELERATING -1

const char* gEndOrResetPrompt = "End or Reset to stop";
const char* gResetPrompt =      "Reset to stop       ";


// State transition signals
enum
{
    MC_SIG_NONE,
    MC_SIG_NEXT,
    MC_SIG_RESET,
    MC_SIG_START,
    MC_SIG_END
};


// I prefer not to have all the state machine nodes referencing the
// global variable, so this single point of access passes it through
// the state machine. The pointer dereferencing may be slower,
// but the code is cleaner this way. This is the OnEnter() for all nodes.
void* MotionControlGetState(void)
{
    return &gMotionControlState;
}


// Generic OnLeave() waits for keypresses to end.
void MotionControlWaitForKeyUp(void* aUserState)
{
    (void)aUserState;
    KeyboardWaitForAllKeysUp();
}



int MotionControlStartNode_DoWork(void* aUserState)
{
    byte key = KEY_NONE;
    int result = MC_SIG_NONE;

    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;

    MotorStop();
    KeyboardQueueClear();

    LCDClear();
    LCDEnableCursor(0);
    LCDEnableCursorBlink(0);
    LCDWriteString("Begin Sequence");
    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
    LCDWriteString("Press Start to begin.");

    do
    {
        key = KeyboardQueuePop();
        switch (key)
        {
            case KEY_START:
                result = MC_SIG_START;
                break;
            case KEY_UP:
                mcState->mMenuResultCode = MENUSIGNAL_PREV; // Feed navigation keys back to menu state machine.
                result = MC_SIG_RESET;
                break;
            case KEY_DOWN:
                mcState->mMenuResultCode = MENUSIGNAL_NEXT; // Feed navigation keys back to menu state machine.
                result = MC_SIG_RESET;
                break;
            default: break;
        }
    } while (result == MC_SIG_NONE);

    return result;
}


// Special OnEnter for taking the picture; records start time for measuring photo interval.
void* MotionControlTakePictureNode_OnEnter(void)
{
    MotionControlState_t* mcState = MotionControlGetState();

    // Reset the timer if the count is getting high (very unlikely).
    if (TimerGetCount() > 0xFF000000L)
    {
        TimerResetCount();
    }

    // Save the current time as the baseline for relative time measurements in other nodes.
    mcState->mBaseTime = TimerGetCount();

    return mcState;
}


// State machine node that takes the picture and waits for the camera to finish.
int MotionControlTakePictureNode_DoWork(void* aUserState)
{
    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;
    char numBuf[10];
    int result = MC_SIG_NONE;
    uint32_t targetTime;
    uint32_t curTime;

    mcState->mPictureCount += 1;

    LCDClear();
    LCDWriteString("Taking pic");
    snprintf(numBuf, 9, "%d", mcState->mPictureCount);
    LCDSetCursorAddress(16 - strlen(numBuf));
    LCDWriteString(numBuf);
    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
    LCDWriteString(mcState->mAbortPrompt);

    // Trip the camera shutter.
    CameraTrigger(); // This blocks for the duration of the trigger pulse.

    // Wait until exposure time has elapsed.
    LCDSetCursorAddress(0);
    LCDWriteString("Waiting ");
    targetTime = TimerGetCount() + (uint32_t)MenuGetExposureDuration();

    do
    {
        curTime = TimerGetCount();
        LCDSetCursorAddress(8);
        snprintf(numBuf, 9, "%ld", targetTime - curTime);
        LCDWriteString(numBuf);
        LCDWriteString("    ");

        if (curTime >= targetTime)
        {
            if (mcState->mEndAfterNextPhoto == TRUE) // Transition to program done node instead of next movement node.
            {
                result = MC_SIG_END;
            }
            else
            {
                result = MC_SIG_NEXT; // Progress to next step.
            }
        }
        else
        {
            switch (KeyboardQueuePop())
            {
                case KEY_RESET: 
                    result = MC_SIG_RESET;
                    break;
                case KEY_END: 
                    mcState->mPhase = PHASE_DECELERATING; // On end pressed, switch to deceleration mode.
                    mcState->mAbortPrompt = gResetPrompt;
                    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
                    LCDWriteString(mcState->mAbortPrompt);
                    break;
                default: break;
            }
        }

    } while (result == MC_SIG_NONE);

    return result;
}


// Do the motor move.
int MotionControlMoveNode_DoWork(void* aUserState)
{
    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;
    char numBuf[10];
    int result = MC_SIG_NONE;
    byte key;
    int moveDone = FALSE;
    int motorTicks = 0UL;
    int rawSensorCount;

    // Compute new velocity in tenths of a mm/frame
    int mmToMove = mcState->mVelocity;
    switch (mcState->mPhase)
    {
        case PHASE_ACCELERATING:
            mmToMove += mcState->mAcceleration;
            if (mmToMove > mcState->mMaxVelocity)
            {
                mmToMove = mcState->mMaxVelocity;
                mcState->mPhase = PHASE_COASTING;
            }
            break;
        case PHASE_DECELERATING:
            mmToMove -= mcState->mAcceleration;
            if (mmToMove < 0)
            {
                mmToMove = 0;
                mcState->mEndAfterNextPhoto = TRUE; // Reached end of move; take one more photo then return to menu.
                mcState->mPhase = PHASE_COASTING;
            }
            break;
        default: break;
    }

    mcState->mVelocity = mmToMove;

    LCDClear();
    LCDWriteString("Moving ");
    snprintf(numBuf, 9, "%d.%d", mmToMove / 10, mmToMove % 10);
    LCDWriteString(numBuf);
    LCDWriteString(" mm");
    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
    LCDWriteString(mcState->mAbortPrompt);

    // Compute number of motor sensor ticks to move the desired distance in mm.
    // The drive shaft has 20 rotations per inch and the motor sensor ticks
    // 16 times per rotation. There are 25.4 mm in one inch.
    // So the conversion is ticks = (320 / 25.4) * mm. Unfortunately the GCF of
    // 3200 and 254 is only 2, meaning a multiplication by 1600 (11 bits) needs to be done
    // to accomplish this with integer math, and it's not unreasable for this
    // robot to support movement velocities of over 32mm (5 bits), so this math needs
    // to be done with 32-bit integers for safety. The result is expected to fit in 15 bits though,
    // since 32768 / 320 * 25.4 = 2600, or 2.6 meters, which is longer than the robot.
    // Note mmToMove is always positive - the movement direction is stored separately.
    // 160 is used instead of 1600 because the move distance is in units of tenths of mm, not mm.
    motorTicks = (int)((160UL * (uint32_t)mmToMove) / 127UL);

    // Start the motor.
    if ((motorTicks > 0) && (mcState->mMotorDirection != MOTOR_IDLE))
    {
        MotorSensorCountReset();
        MotorStart(mcState->mMotorDirection); // This handles the MOTOR_IDLE case transparently.

        while (moveDone == FALSE)
        {
            rawSensorCount = MotorGetSensorCount();
            if (rawSensorCount < 0)
            {
                rawSensorCount = -rawSensorCount;  // We care only about distance traveled here, not direction.
            }

            if (rawSensorCount >= motorTicks)
            {
                moveDone = TRUE;
            }

            key = KeyboardQueuePop();
            switch (key)
            {
                case KEY_END:
                    mcState->mPhase = PHASE_DECELERATING; // On end pressed, switch to deceleration mode.
                    mcState->mAbortPrompt = gResetPrompt;
                    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
                    LCDWriteString(mcState->mAbortPrompt);
                    break;
                case KEY_RESET:
                    result = MC_SIG_RESET;
                    moveDone = TRUE;
                    break;
                default: break;
            }
        }

        // Stop command entered or target distance reached; stop the motor.
        MotorStop();
    }

    if (result == MC_SIG_NONE)
    {
        result = MC_SIG_NEXT; // If no keys were pressed or the move was unnecessary, move on to the settle time state.
    }

    return result;
}


// Waits for the movement settle time to elapse after the motor move.
int MotionControlSettleNode_DoWork(void* aUserState)
{
    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;
    int result = MC_SIG_NONE;
    byte key;
    uint32_t stopTime;
    char numBuf[10];

    LCDClear();
    LCDWriteString("Waiting ");
    snprintf(numBuf, 9, "%d", mcState->mMotorSettleTime);
    LCDWriteString(numBuf);
    LCDWriteString(" mS");
    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
    LCDWriteString(mcState->mAbortPrompt);

    // If we're not ordered to stop, wait for the settle time to elapse.
    stopTime = TimerGetCount();
    while (result == MC_SIG_NONE)
    {
        if (TimerGetCount() > stopTime + (uint32_t)mcState->mMotorSettleTime)
        {
            result = MC_SIG_NEXT; // Move on to next state once settle time elapses
        }

        key = KeyboardQueuePop();
        switch (key)
        {
            case KEY_END:
                mcState->mPhase = PHASE_DECELERATING; // On end pressed, switch to deceleration mode.
                mcState->mAbortPrompt = gResetPrompt;
                LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
                LCDWriteString(mcState->mAbortPrompt);
                break;
            case KEY_RESET:
                result = MC_SIG_RESET;
                break;
            default: break;
        }
    }

    return result;
}


int MotionControlPhotoIntervalNode_DoWork(void* aUserState)
{
    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;
    int result = MC_SIG_NONE;
    char numBuf[10];
    byte key;
    uint32_t curTime;
    uint32_t targetTime = mcState->mBaseTime + (uint32_t)mcState->mPhotoInterval;

    LCDClear();
    LCDWriteString("Next pic: ");
    LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
    LCDWriteString(mcState->mAbortPrompt);

    // If we're not ordered to stop, wait for the settle time to elapse.
    while (result == MC_SIG_NONE)
    {
        curTime = TimerGetCount();
        snprintf(numBuf, 9, "%ld", targetTime - curTime);
        LCDSetCursorAddress(10);
        LCDWriteString(numBuf);
        LCDWriteString(" mS  ");

        if (curTime >= targetTime)
        {
            result = MC_SIG_NEXT; // Take next photo.
        }

        key = KeyboardQueuePop();
        switch (key)
        {
            case KEY_END:
                mcState->mPhase = PHASE_DECELERATING; // On end pressed, switch to deceleration mode.
                mcState->mAbortPrompt = gResetPrompt;
                LCDSetCursorAddress(TEENSY_HD44780_LINE2_START);
                LCDWriteString(mcState->mAbortPrompt);
                break;
            case KEY_RESET:
                result = MC_SIG_RESET;
                break;
            default: break;
        }
    }

    return result;
}


int MotionControlDoneNode_DoWork(void* aUserState)
{
    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;
    LCDClear();
    LCDWriteString("PROGRAM COMPLETE");
    _delay_ms(3000);
    mcState->mMenuResultCode = MENUSIGNAL_REDO; // Restart the run program menu.
    return MC_SIG_NEXT;
}


// Briefly displays a message acknowledging you've aborted the run.
int MotionControlAbortNode_DoWork(void* aUserState)
{
    MotionControlState_t* mcState = (MotionControlState_t*)aUserState;
    MotorStop(); // Not actually needed, but included for safety.
    LCDClear();
    LCDWriteString("ABORTING");
    _delay_ms(3000);
    mcState->mMenuResultCode = MENUSIGNAL_REDO; // Restart the run program menu.
    return MC_SIG_NEXT;
}


// There are no functions needed for the terminal node of the state machine;
// the motion control main loop checks for arrival at that state.


// ----- State machine definition tables.

// Remember to update the enums to match the state table below.
enum
{
    MC_START_NODE,
    MC_TAKE_PICTURE_NODE,
    MC_MOVE_NODE,
    MC_SETTLE_NODE,
    MC_INTERVAL_NODE,
    MC_DONE_NODE,
    MC_ABORT_NODE,
    MC_FINISHED_NODE,
    NUM_MC_NODES
};


StateMachineNextState_t gStartNodeTransitions[] = 
{
    { MC_SIG_RESET, MC_FINISHED_NODE },
    { MC_SIG_START, MC_TAKE_PICTURE_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gTakePictureNodeTransitions[] =
{
    { MC_SIG_END, MC_DONE_NODE },
    { MC_SIG_NEXT, MC_MOVE_NODE },
    { MC_SIG_RESET, MC_ABORT_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gMoveNodeTransitions[] =
{
    { MC_SIG_NEXT, MC_SETTLE_NODE },
    { MC_SIG_RESET, MC_ABORT_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gSettleNodeTransitions[] =
{
    { MC_SIG_NEXT, MC_INTERVAL_NODE },
    { MC_SIG_RESET, MC_ABORT_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gIntervalNodeTransitions[] =
{
    { MC_SIG_NEXT, MC_TAKE_PICTURE_NODE },
    { MC_SIG_RESET, MC_ABORT_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gDoneNodeTransitions[] =
{
    { MC_SIG_NEXT, MC_FINISHED_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gAbortNodeTransitions[] =
{
    { MC_SIG_NEXT, MC_FINISHED_NODE },
    TRANSITIONTABLE_END
};

StateMachineNextState_t gFinishedNodeTransitions[] =
{
    TRANSITIONTABLE_END
};


StateMachineNode_t gMotionControlStateMachineNodes[] =
{  // OnEnter()                                 DoWork()                                OnLeave()                       transition table
    { MotionControlGetState,                    MotionControlStartNode_DoWork,          MotionControlWaitForKeyUp,      gStartNodeTransitions },
    { MotionControlTakePictureNode_OnEnter,     MotionControlTakePictureNode_DoWork,    MotionControlWaitForKeyUp,      gTakePictureNodeTransitions },
    { MotionControlGetState,                    MotionControlMoveNode_DoWork,           MotionControlWaitForKeyUp,      gMoveNodeTransitions },
    { MotionControlGetState,                    MotionControlSettleNode_DoWork,         MotionControlWaitForKeyUp,      gSettleNodeTransitions },
    { MotionControlGetState,                    MotionControlPhotoIntervalNode_DoWork,  MotionControlWaitForKeyUp,      gIntervalNodeTransitions },
    { MotionControlGetState,                    MotionControlDoneNode_DoWork,           MotionControlWaitForKeyUp,      gDoneNodeTransitions },
    { MotionControlGetState,                    MotionControlAbortNode_DoWork,          MotionControlWaitForKeyUp,      gAbortNodeTransitions },
    { NULL,                                     NULL,                                   MotionControlWaitForKeyUp,      gFinishedNodeTransitions }
};


// Initializes the user's state machine data structure with the motion control state machine.
void MotionControlInit(StateMachineState_t* aStateMachine)
{
    gMotionControlState.mMenuResultCode = MENUSIGNAL_NONE;
    gMotionControlState.mPictureCount = 0;
    gMotionControlState.mPhase = PHASE_ACCELERATING;
    gMotionControlState.mVelocity = 10 * MenuGetInitialVelocity(); // mm to tenths of mm to match acceleration units.
    gMotionControlState.mAcceleration = MenuGetAcceleration();
    gMotionControlState.mMaxVelocity = 10 * MenuGetMaxVelocity();
    gMotionControlState.mMotorSettleTime = MenuGetSettleTime();
    gMotionControlState.mMotorDirection = MenuGetMotorDirection();
    gMotionControlState.mEndAfterNextPhoto = FALSE;
    gMotionControlState.mPhotoInterval = MenuGetPhotoInterval();
    gMotionControlState.mAbortPrompt = gEndOrResetPrompt;

    // Since the camera trigger pulse is typically very short, it will
    // be controlled by a blocking function instead of adding another
    // delay state to the motion control state machine.
    CameraSetTriggerPulseWidth(MenuGetTriggerPulseDuraction());

    StateMachineInit(gMotionControlStateMachineNodes, NUM_MC_NODES, MC_START_NODE, aStateMachine);
}


// Runs the motion control state machine.
int MotionControlUpdate(StateMachineState_t* aStateMachine)
{
    int currentNode = 0;
    int result = MENUSIGNAL_NONE;

    currentNode = StateMachineUpdate(aStateMachine);
    if (currentNode == MC_FINISHED_NODE) // State machine has terminated.
    {
        result = gMotionControlState.mMenuResultCode;
    }
    else if (currentNode < 0) // State machine error
    {
        result = currentNode; // Propagate errors up to menu state machine.
    }
    else if (currentNode >= NUM_MC_NODES)
    {
        result = STATEMACHINE_INVALID; // Force error - we've got a bad transition.
    }

    return result;
}
