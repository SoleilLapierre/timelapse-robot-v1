// Copyright (c) 2015 by Soleil Lapierre

#include "motor.h"
#include <util/delay.h>

// Note Port F is shared with the keyboard scanner.
#define MOTOR_DDR DDRF
#define MOTOR_PORT_WRITE PORTF
#define MOTOR_PORT_READ PINF

// Bits 5 & 6 control the motor direction - both high to disable, one or the other low to move.
// Bit 5 goes to input 1 of the motor controller and bit 6 to input 2.
// Bit 7 in the rotation sensor input.
#define MOTOR_DIR_1     0x20
#define MOTOR_DIR_2     0x40
#define MOTOR_SENSOR    0x80
#define MOTOR_OUTPUT_MASK (MOTOR_DIR_1 | MOTOR_DIR_2)
#define MOTOR_INPUT_MASK MOTOR_SENSOR
#define MOTOR_BITS (MOTOR_INPUT_MASK | MOTOR_OUTPUT_MASK)

// Number of mS to wait for the motor to stop when reversing direction.
#define REVERSE_DELAY 100

volatile int gMotorSensorCount = 0;
volatile int gMotorDirection = MOTOR_IDLE;
volatile int gLastSensorState = 0;


void MotorInit(void)
{
	MOTOR_DDR &= ~(MOTOR_BITS);								// Set all motor pins to input mode so we can...
	// ... turn on the pullup resistor for the sensor input. This is needed
	// because of a component selection error in the current prototype; the
	// optical sensor that detects motor rotation has an open-collector output.
	// I would have preferred to use a non-OC part, but this will work well enough.
    MOTOR_PORT_WRITE = (MOTOR_PORT_WRITE & ~MOTOR_BITS) | MOTOR_INPUT_MASK;

	MOTOR_DDR |= MOTOR_OUTPUT_MASK;						    // Set lower 2 pins to output.
	MOTOR_PORT_WRITE |= MOTOR_OUTPUT_MASK;				    // Set all outputs high (disables motor).
}


void MotorStart(int aDirection)
{
	if (aDirection == MOTOR_TOWARDS)
	{
		if (gMotorDirection == MOTOR_AWAY)
		{
			MotorStop();
			_delay_ms(REVERSE_DELAY); 
		}

        MOTOR_PORT_WRITE = (MOTOR_PORT_WRITE & ~MOTOR_OUTPUT_MASK) | MOTOR_DIR_2; // In1 = 0, In2 = 1 for towards.
		gMotorDirection = aDirection;
	}
	else if (aDirection == MOTOR_AWAY)
	{
		if (gMotorDirection == MOTOR_TOWARDS)
		{
			MotorStop();
			_delay_ms(REVERSE_DELAY);
		}

        MOTOR_PORT_WRITE = (MOTOR_PORT_WRITE & ~MOTOR_OUTPUT_MASK) | MOTOR_DIR_1; // In1 = 1, In2 = 0 for away.
		gMotorDirection = aDirection;
	}
}


void MotorStop(void)
{
	MOTOR_PORT_WRITE |= MOTOR_OUTPUT_MASK; // Motor enable = 11 (no movement).
	gMotorDirection = MOTOR_IDLE;
}


void MotorSensorPoll(void)
{
	int sensorState = ((MOTOR_PORT_READ & MOTOR_SENSOR) != 0) ? 1 : 0;
	if (sensorState != gLastSensorState) // Bump count on both rising and falling edges.
	{
		gMotorSensorCount += gMotorDirection;
	}

	gLastSensorState = sensorState;
}


int MotorGetSensorCount(void)
{
	return gMotorSensorCount;
}


void MotorSensorCountReset(void)
{
    gMotorSensorCount = 0;
}
