// Copyright (c) 2015 by Soleil Lapierre

#include "keyboard.h"
#include "types.h"

#define KEYBOARD_DDR DDRF
#define KEYBOARD_PORT_WRITE PORTF
#define KEYBOARD_PORT_READ PINF

// Bits 0 and 1 are outputs that drive the 4 keyboard columns. 
// 0 = leftmost column, 3 = rightmost.
#define KEYBOARD_COL_MASK 0x03

// Bits 2,3 and 4 are inputs from the encoder that detects presses
// on the rows of the keyboard.
// 0 = top row, increasing downwards.
#define KEYBOARD_ROW_MASK 0x1C

#define MAX_KEYS 32

// State flags for keyboard state array.
// Since there are 4 rows at at most 8 columns in the keyboard,
// that means at most 32 keys. Unfortunately can't use a
// bitfield because this uC only has 16-bit registers.
volatile unsigned char gKeysDownThisPass[MAX_KEYS]; // Keys that were newly pressed this scan pass.
volatile unsigned char gKeyState[MAX_KEYS];        // All keys that are definitely currently held down (debounced).

#define KEY_QUEUE_LENGTH 16
volatile unsigned char gKeyQueue[KEY_QUEUE_LENGTH]; // Input buffer - key IDs are queued here as keypresses get debounced.
volatile int gKeyQueueHead = 0; // Circular buffer head and tail pointers - if equal, queue is empty.
volatile int gKeyQueueTail = 0;
int gKeyQueueEnabled = 0;

unsigned char gCurrentColumn = 0;


void KeyboardInit(int aEnableQueue)
{
	int i;
	for (i = 0; i < MAX_KEYS; ++i)
	{
		gKeysDownThisPass[i] = 0;
		gKeyState[i] = 0;
	}

	KEYBOARD_DDR &= ~(KEYBOARD_ROW_MASK | KEYBOARD_COL_MASK);	// Set all keyboard pins to input mode so we can...
	KEYBOARD_PORT_WRITE &= KEYBOARD_COL_MASK;			        // Turn off pullup resistors for lower 2 output bits.
	KEYBOARD_DDR |= KEYBOARD_COL_MASK;						    // Set lower 2 pins to output.
	KEYBOARD_PORT_WRITE &= ~KEYBOARD_COL_MASK;				    // Set all outputs low.
	KEYBOARD_PORT_WRITE |= (gCurrentColumn & KEYBOARD_COL_MASK); // Output first column to scan.
	gKeyQueueEnabled = (aEnableQueue != 0) ? TRUE : FALSE;
}


void KeyboardUpdate(void)
{
	unsigned char row = KEYBOARD_PORT_READ & KEYBOARD_ROW_MASK;
	unsigned char currentKey = 0xFF;
	int i;

	// See if any keys in the current column are pressed.
	if (row != KEYBOARD_ROW_MASK) // Any low bit indicates a keypress on the current column.
	{
		currentKey = row | gCurrentColumn;
		gKeysDownThisPass[currentKey] = TRUE;
	}

	// Output next column scan pulse.
	gCurrentColumn = (gCurrentColumn + 1) & KEYBOARD_COL_MASK;
	KEYBOARD_PORT_WRITE = (KEYBOARD_PORT_WRITE & ~KEYBOARD_COL_MASK) | (gCurrentColumn & KEYBOARD_COL_MASK);

	// If we just completed a scan, handle debouncing.
	if (gCurrentColumn == 0)
	{
		if (gKeyQueueEnabled == TRUE)
		{
			for (i = 0; i < 32; ++i) // Note order is not guaranteed if two keys are pressed simultaneously.
			{
				if ((gKeysDownThisPass[i] != 0) && (gKeyState[i] == 0)) // New key pressed; add it to output buffer.
				{
					if (((gKeyQueueTail + 1) % KEY_QUEUE_LENGTH) != gKeyQueueHead) // If there is space in the queue.
					{
						gKeyQueue[gKeyQueueTail] = (unsigned char)i;
						gKeyQueueTail = (gKeyQueueTail + 1) % KEY_QUEUE_LENGTH;
					}
				}
			}
		}

		for (i = 0; i < MAX_KEYS; ++i)
		{
			gKeyState[i] = gKeysDownThisPass[i];
			gKeysDownThisPass[i] = 0;
		}
	}
}


int KeyboardIsKeyPressed(int aKey)
{
	return gKeyState[aKey % MAX_KEYS];
}


int KeyboardIsAnyKeyPressed(void)
{
	for (int i = 0; i < MAX_KEYS; ++i)
	{
		if (gKeyState[i] != 0)
		{
			return TRUE;
		}
	}

	return FALSE;
}


unsigned char KeyboardQueuePeek(void)
{
	unsigned char result = KEY_NONE;
	if ((gKeyQueueEnabled != 0) && (gKeyQueueHead != gKeyQueueTail))
	{
		result = gKeyQueue[gKeyQueueHead];
	}

	return result;
}


unsigned char KeyboardQueuePop(void)
{
	unsigned char result = KEY_NONE;
	if ((gKeyQueueEnabled != 0) && (gKeyQueueHead != gKeyQueueTail))
	{
		result = gKeyQueue[gKeyQueueHead];
		gKeyQueueHead = (gKeyQueueHead + 1) % KEY_QUEUE_LENGTH;
	}

	return result;
}


void KeyboardQueueClear(void)
{
	gKeyQueueHead = gKeyQueueTail;
}


unsigned char KeyboardNumericValue(unsigned char aCode)
{
	unsigned char result = KEY_NONE;
	
	// Key scancodes are noncontiguous and nonmonotonic, so I'm using
	// a switch statement to convert them to numeric values.
	switch (aCode)
	{
		case KEY_0: result = 0; break;
		case KEY_1: result = 1; break;
		case KEY_2: result = 2; break;
		case KEY_3: result = 3; break;
		case KEY_4: result = 4; break;
		case KEY_5: result = 5; break;
		case KEY_6: result = 6; break;
		case KEY_7: result = 7; break;
		case KEY_8: result = 8; break;
		case KEY_9: result = 9; break;
		default: break;
	}

	return result;
}


void KeyboardWaitForAllKeysUp(void)
{
    while (KeyboardIsAnyKeyPressed() == TRUE) {}
}