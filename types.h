// Copyright (c) 2015 by Soleil Lapierre

#ifndef _TYPES_H_
#define _TYPES_H_

typedef unsigned char byte;

#ifndef NULL
#define NULL (void*)0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#endif
