// Copyright (c) 2015 by Soleil Lapierre

// Converts a fixed-point signed integer to a decimal ASCII string, with
// minus sign and decimal point as appropriate. Precision is the number of digits to
// put after the decimal point.
void FixedToString(int aValue, int aPrecision, char* aOutput)
{
    char buf[16]; // More than enough for even 32-bit ints.
    int i = 0, j = 0;
    int val = aValue;

    // Add minus sign if needed.
    if (aValue < 0)
    {
        aOutput[j++] = '-';
        val = -aValue;
    }

    // Convert to decimal in reverse digit order first.
    while (val > 0)
    {
        buf[i++] = '0' + (val % 10);
        val /= 10;
    }

    --i; // Point to first decimal digit if any.

    // Add leading zero if needed.
    if ((i < 0) || (i == aPrecision - 1))
    {
        aOutput[j++] = '0';
    }

    // Print digits before the decimal place.
    while ((i >= aPrecision) && (i >= 0))
    {
        aOutput[j++] = buf[i--];
    }

    // Decimal point if any.
    if (aPrecision > 0)
    {
        aOutput[j++] = '.';
    }

    // Digits after decimal point, if any.
    while (aPrecision > 0)
    {
        if (i >= 0)
        {
            aOutput[j++] = buf[i--];
        }
        else
        {
            aOutput[j++] = '0';
        }

        --aPrecision;
    }

    // Add null terminator.
    aOutput[j] = '\0';
}


// Naive conversion of ASCII string to signed fixed-point number.
// This ignores the positioning of the decimal point. It assumes
// the decimal point is wherever it is supposed to be for the intended
// precision and just dumbly converts all the digits it finds.
int StringToFixed(const char* aBuffer)
{
    int value = 0;
    int i = 0;
    char c;

    while (aBuffer[i] != '\0')
    {
        c = aBuffer[i];
        if (c == '-')
        {
            if (value >= 0)
            {
                value = -value; // Only accept one minus sign. Note this doesn't care about the placement.
            }
        }
        else if ((c >= '0') && (c <= '9'))
        {
            value *= 10;
            value += c - '0';
        }

        i++;
    }

    return value;
}
