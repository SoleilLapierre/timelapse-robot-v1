// Copyright (c) 2015 by Soleil Lapierre

#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <avr/io.h>

// Names for key codes. 
// ALso obviously these names are specific to the labels on the particular keyboard for this project.
#define KEY_NONE 0xFF
#define KEY_0 0x05
#define KEY_1 0x10
#define KEY_2 0x11
#define KEY_3 0x12
#define KEY_4 0x0C
#define KEY_5 0x0D
#define KEY_6 0x0E
#define KEY_7 0x08
#define KEY_8 0x09
#define KEY_9 0x0A
#define KEY_UP 0x06
#define KEY_DOWN 0x02
#define KEY_START 0x13
#define KEY_LEFT 0x01
#define KEY_RIGHT 0x03
#define KEY_END 0x0F
#define KEY_ENTER 0x07
#define KEY_CLEAR 0x04
#define KEY_RESET 0x00
#define KEY_UNUSED 0x0B


// Called once at program start.
// Pass in 1 to enable the input queue, or 0 if you're only going to use KeyboardIsKeyPressed().
void KeyboardInit(int aEnableQueue);

// Should be called at least tens of times per second - each call scans one column
// of the keybuard, four calls are needed for a full scan, and two full scans
// are needed to debounce each key.
void KeyboardUpdate(void);

// returns 1 if the specified key is currently pressed, KEY_NONE otherwise.
int KeyboardIsKeyPressed(int aKey);
int KeyboardIsAnyKeyPressed(void);

// Look at the next key in the queue without removing it.
unsigned char KeyboardQueuePeek(void);

// Pop the next key from the queue. Returns KEY_NONE if there are none.
unsigned char KeyboardQueuePop(void);

// Discard entire contents of keyboard input queue.
void KeyboardQueueClear(void);

// Returns KEY_NONE if the key code is not numeric, or the numeric value if it is.
unsigned char KeyboardNumericValue(unsigned char aCode);

// Blocks until all keys are released.
void KeyboardWaitForAllKeysUp(void);

#endif
